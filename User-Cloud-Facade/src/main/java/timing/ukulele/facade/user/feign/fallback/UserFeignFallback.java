package timing.ukulele.facade.user.feign.fallback;

import org.springframework.web.bind.annotation.RequestBody;
import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.user.view.PasswordVO;
import timing.ukulele.data.user.view.UserVO;
import timing.ukulele.facade.user.feign.UserFeignFacade;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserFeignFallback implements UserFeignFacade {
    @Override
    public ResponseData<UserVO> getCurrentUser(String currentUser) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<UserVO> getUserByUserName(String userName) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<UserVO> getUserByPhone(String phone) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<UserVO> getUserByPhoneOrName(String param) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<UserVO>> getUserByParam(Map<String, Object> params) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<UserVO>> getByIds(Set<Long> ids) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<UserVO> user(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<UserVO> userInfo(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> userDel(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> user(String currentUser, UserVO user) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> userUpdate(String currentUser, UserVO user) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> perfectUserInfo(UserVO vo, String username) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Map<String, Object>> getUserSecurityInfo(String username) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> sendEmailCode(String username, String email) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> bindEmail(String username, String email, String code) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> changePassword(String username, @RequestBody PasswordVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> setUserDefaultTenant(String current, String username, Long userId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<UserVO>> batchCreateNotExist(String current, List<UserVO> user) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> bindOtp(Long userId, String secret) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<String> clearOtp(String current, Long userId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

}
