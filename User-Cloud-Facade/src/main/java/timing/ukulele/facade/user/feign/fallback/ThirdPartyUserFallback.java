package timing.ukulele.facade.user.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.user.view.ThirdPartyUserVO;
import timing.ukulele.facade.user.feign.ThirdPartyUserFeignFacade;

public class ThirdPartyUserFallback implements ThirdPartyUserFeignFacade {
    @Override
    public ResponseData<ThirdPartyUserVO> getUserByThirdInfo(String platId, Integer plat) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<ThirdPartyUserVO> getThirdInfoByUserId(Long userId, Integer type) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> updateThirdInfo(ThirdPartyUserVO var1) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addThirdInfo(ThirdPartyUserVO var1) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
