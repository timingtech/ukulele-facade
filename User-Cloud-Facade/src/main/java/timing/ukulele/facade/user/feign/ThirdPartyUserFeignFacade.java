package timing.ukulele.facade.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.user.IThirdPartyUserFacade;
import timing.ukulele.facade.user.feign.factory.UserFallbackFactory;

@FeignClient(contextId = "thirdPartyUserFeignFacade", name = "ukulele-user", path = "/user/thirdParty",
        fallbackFactory = UserFallbackFactory.class)
public interface ThirdPartyUserFeignFacade extends IThirdPartyUserFacade {
}
