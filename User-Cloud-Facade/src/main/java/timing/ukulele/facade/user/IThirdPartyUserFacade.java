package timing.ukulele.facade.user;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.user.view.ThirdPartyUserVO;

public interface IThirdPartyUserFacade {

    /**
     * 查询第三方用户
     *
     * @param platId 第三方平台用户标识 如openid
     * @param plat   第三方平台类型，1小程序
     * @return 第三方用户信息
     */
    @GetMapping("/{platId}/{plat}")
    ResponseData<ThirdPartyUserVO> getUserByThirdInfo(@PathVariable("platId") String platId, @PathVariable("plat") Integer plat);

    /**
     * 根据用户id获取指定平台的用户信息
     *
     * @param userId
     * @param type
     * @return
     */
    @GetMapping("/plat/user/id")
    ResponseData<ThirdPartyUserVO> getThirdInfoByUserId(@RequestParam("userId") Long userId, @RequestParam("type") Integer type);

    @PutMapping({"/update"})
    ResponseData<Boolean> updateThirdInfo(@RequestBody ThirdPartyUserVO var1);

    @PostMapping({"/add"})
    ResponseData<Boolean> addThirdInfo(@RequestBody ThirdPartyUserVO var1);
}
