package timing.ukulele.facade.user;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.user.view.PasswordVO;
import timing.ukulele.data.user.view.UserVO;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface IUserFacade {

    @GetMapping("/current")
    ResponseData<UserVO> getCurrentUser(@RequestHeader("X-USER") String currentUser);

    /**
     * 根据用户名查询用户
     *
     * @param userName 用户名
     * @return 用户信息
     */
    @GetMapping("/name/{username}")
    ResponseData<UserVO> getUserByUserName(@PathVariable("username") String userName);

    /**
     * 根据电话号码查询用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    @GetMapping("/phone/{phone}")
    ResponseData<UserVO> getUserByPhone(@PathVariable("phone") String phone);

    /**
     * 根据用户名或手机号等唯一可确定身份的参数查找用户
     *
     * @param param 用户名 或手机号等唯一可确定身份的参数
     * @return 用户信息
     */
    @GetMapping("/param/{param}")
    ResponseData<UserVO> getUserByPhoneOrName(@PathVariable("param") String param);

    /**
     * 根据参数获取用户列表
     *
     * @param params 参数集合
     * @return 用户结合
     */
    @GetMapping("/getByParam")
    ResponseData<List<UserVO>> getUserByParam(@RequestParam("params") Map<String, Object> params);

    /**
     * 根据id数组批量查询用户信息
     *
     * @param ids id数组
     * @return
     */
    @GetMapping("/getByIds")
    ResponseData<List<UserVO>> getByIds(@RequestParam("ids") Set<Long> ids);

    /**
     * 通过ID查询当前用户信息，不带密码
     *
     * @param id ID
     * @return 用户信息
     */
    @GetMapping("/{id}")
    ResponseData<UserVO> user(@PathVariable(value = "id") Long id);

    /**
     * 通过ID查询当前用户信息,带密码
     *
     * @param id ID
     * @return 用户信息
     */
    @GetMapping("/all/{id}")
    ResponseData<UserVO> userInfo(@PathVariable(value = "id") Long id);

    /**
     * 删除用户信息
     *
     * @param id ID
     * @return R
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> userDel(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    /**
     * 添加用户
     *
     * @param user 用户信息
     * @return success/false
     */
    @PostMapping
    ResponseData<Boolean> user(@RequestHeader("X-USER") String currentUser, @RequestBody UserVO user);

    /**
     * 更新用户信息
     *
     * @param user 用户信息
     * @return R
     */
    @PutMapping
    ResponseData<Boolean> userUpdate(@RequestHeader("X-USER") String currentUser, @RequestBody UserVO user);

    /**
     * 完善信息
     *
     * @param vo       用户信息
     * @param username 当前用户
     * @return
     */
    @PutMapping("/perfect")
    ResponseData<Boolean> perfectUserInfo(@RequestBody UserVO vo, @RequestHeader("X-USER") String username);

    /**
     * 当前用户的安全信息
     *
     * @param username 当前用户
     * @return
     */
    @GetMapping("/security")
    ResponseData<Map<String, Object>> getUserSecurityInfo(@RequestHeader("X-USER") String username);

    /**
     * 绑定邮箱前的操作，给邮箱发送验证码
     *
     * @param username 当前用户
     * @param email    邮箱地址
     * @return
     */
    @PostMapping("/email/code")
    ResponseData<String> sendEmailCode(@RequestHeader("X-USER") String username, @RequestParam("email") String email);

    /**
     * 绑定邮箱
     *
     * @param username 当前用户
     * @param email    邮箱地址
     * @param code     验证码
     * @return
     */
    @PutMapping("/email/bind")
    ResponseData<String> bindEmail(@RequestHeader("X-USER") String username, @RequestParam("email") String email, @RequestParam("code") String code);

    /**
     * 修改密码
     *
     * @param username 当前用户
     * @param oldPwd   老密码
     * @param newPwd   新密码
     * @return
     */
    @PutMapping("/password/change")
    ResponseData<String> changePassword(@RequestHeader("X-USER") String username, @RequestBody PasswordVO vo);


    /**
     * 设置用户默认租户
     *
     * @param username 用户名
     * @param userId   用户id
     * @param tenantId 租户id
     * @return
     */
    @PutMapping("/tenant/default")
    ResponseData<String> setUserDefaultTenant(@RequestHeader("X-USER") String current, @RequestParam(value = "username", required = false) String username, @RequestParam(value = "userId", required = false) Long userId, @RequestParam("tenantId") Long tenantId);

    /**
     * 批量添加用户（如果存在，那么返回用户信息）
     *
     * @param user 用户信息
     * @return success/false
     */
    @PostMapping("/batchCreateNotExist")
    ResponseData<List<UserVO>> batchCreateNotExist(@RequestHeader("X-USER") String current, @RequestBody List<UserVO> user);

    /**
     * 启用用户的OTP
     *
     * @param userId 用户ID
     * @param secret OTP密钥
     * @return 成功/失败，以及原因
     */
    @PostMapping("/bindOtp")
    ResponseData<String> bindOtp(@RequestParam("userId") Long userId, @RequestParam("secret") String secret);


    /**
     * 清除用户OTP
     *
     * @param current 当前用户
     * @param userId  用户ID
     * @return 成功/失败，以及原因
     */
    @PostMapping("/clearOtp")
    ResponseData<String> clearOtp(@RequestHeader("X-USER") String current, @RequestParam("userId") Long userId);
}
