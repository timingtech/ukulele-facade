package timing.ukulele.facade.user.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import timing.ukulele.facade.user.feign.factory.ThirdPartyUserFallbackFactory;
import timing.ukulele.facade.user.feign.factory.UserFallbackFactory;

@Configuration
public class UserFeignAutoConfiguration {
    @Bean
    public ThirdPartyUserFallbackFactory thirdPartyUserFallbackFactory() {
        return new ThirdPartyUserFallbackFactory();
    }

    @Bean
    public UserFallbackFactory userFallbackFactory() {
        return new UserFallbackFactory();
    }
}
