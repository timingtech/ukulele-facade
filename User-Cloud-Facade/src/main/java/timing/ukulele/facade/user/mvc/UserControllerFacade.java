package timing.ukulele.facade.user.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.user.IUserFacade;

@RequestMapping("/user")
public interface UserControllerFacade extends IUserFacade {

}
