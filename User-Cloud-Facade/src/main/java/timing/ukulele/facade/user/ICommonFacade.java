package timing.ukulele.facade.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import timing.ukulele.common.data.CaptchaResult;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.user.view.PasswordVO;

public interface ICommonFacade {
    @GetMapping("/captcha/forgot")
    ResponseData<CaptchaResult> getForgotCaptcha();

    @PostMapping("/password/forget/code")
    ResponseData<String> sendForgotPasswordEmailCode(@RequestParam("email") String email, @RequestParam("code") String code);

    @PostMapping("/password/reset")
    ResponseData<String> passwordReset(@RequestBody PasswordVO vo);
}
