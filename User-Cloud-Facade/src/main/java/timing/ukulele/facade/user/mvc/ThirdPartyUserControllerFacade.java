package timing.ukulele.facade.user.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.user.IThirdPartyUserFacade;

@RequestMapping("/thirdParty")
public interface ThirdPartyUserControllerFacade extends IThirdPartyUserFacade {
}
