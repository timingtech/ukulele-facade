package timing.ukulele.facade.user.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.user.feign.UserFeignFacade;
import timing.ukulele.facade.user.feign.fallback.UserFeignFallback;

@Slf4j
public class UserFallbackFactory implements FallbackFactory<UserFeignFacade> {
    @Override
    public UserFeignFacade create(Throwable cause) {
        log.error(cause.getMessage());
        return new UserFeignFallback();
    }
}
