package timing.ukulele.facade.user.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.user.ICommonFacade;

@RequestMapping("/common")
public interface CommonControllerFacade extends ICommonFacade {
}
