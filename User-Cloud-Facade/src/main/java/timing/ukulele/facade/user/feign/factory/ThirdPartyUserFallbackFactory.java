package timing.ukulele.facade.user.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.user.feign.fallback.ThirdPartyUserFallback;

@Slf4j
public class ThirdPartyUserFallbackFactory implements FallbackFactory<ThirdPartyUserFallback> {
    @Override
    public ThirdPartyUserFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new ThirdPartyUserFallback();
    }
}
