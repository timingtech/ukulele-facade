package timing.ukulele.facade.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.user.IUserFacade;
import timing.ukulele.facade.user.feign.factory.UserFallbackFactory;

@FeignClient(contextId = "userFeignFacade", name = "ukulele-user", path = "/user/user",
        fallbackFactory = UserFallbackFactory.class)
public interface UserFeignFacade extends IUserFacade {
}
