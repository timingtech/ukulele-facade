package timing.ukulele.facade.auth.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.auth.persistent.OAuthClientDetailsModel;
import timing.ukulele.facade.auth.feign.ClientFeignFacade;

import java.util.List;
import java.util.Map;

public class ClientFallback implements ClientFeignFacade {
    @Override
    public ResponseData<List<OAuthClientDetailsModel>> getClientByParam(Map<String, Object> params) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<OAuthClientDetailsModel> get(Integer id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> add(OAuthClientDetailsModel client) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> delete(String id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> edit(OAuthClientDetailsModel client) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
