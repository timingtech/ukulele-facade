package timing.ukulele.facade.auth.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.auth.IClientFacade;
import timing.ukulele.facade.auth.feign.factory.ClientFallbackFactory;

@FeignClient(contextId = "clientFeignFacade", name = "ukulele-auth", path = "/auth/client",
        fallbackFactory = ClientFallbackFactory.class)
public interface ClientFeignFacade extends IClientFacade {

}
