package timing.ukulele.facade.auth.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.auth.IClientFacade;

@RequestMapping("/client")
public interface ClientControllerFacade extends IClientFacade {

}
