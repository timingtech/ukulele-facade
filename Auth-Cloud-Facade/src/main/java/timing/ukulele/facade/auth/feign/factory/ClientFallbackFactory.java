package timing.ukulele.facade.auth.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.auth.feign.ClientFeignFacade;
import timing.ukulele.facade.auth.feign.fallback.ClientFallback;

@Slf4j
public class ClientFallbackFactory implements FallbackFactory<ClientFeignFacade> {
    @Override
    public ClientFeignFacade create(Throwable cause) {
        log.error(cause.getMessage());
        return new ClientFallback();
    }
}
