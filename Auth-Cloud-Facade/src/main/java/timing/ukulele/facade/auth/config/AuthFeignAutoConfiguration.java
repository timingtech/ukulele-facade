package timing.ukulele.facade.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import timing.ukulele.facade.auth.feign.factory.ClientFallbackFactory;

@Configuration
public class AuthFeignAutoConfiguration {
    @Bean
    public ClientFallbackFactory clientFallbackFactory() {
        return new ClientFallbackFactory();
    }
}
