package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysParamVO;
import timing.ukulele.facade.portal.feign.SysParamFeignFacade;

import java.util.List;
import java.util.Map;

public class SysParamFallback implements SysParamFeignFacade {
    @Override
    public ResponseData<SysParamVO> get(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysParamVO>> getParamByParam(Map<String, Object> params) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<SysParamVO> getParamByKey(String key, Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> add(String currentUser, SysParamVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> update(String currentUser, SysParamVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> delete(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
