package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.IndexFallback;

@Slf4j
public class IndexFallbackFactory implements FallbackFactory<IndexFallback> {

    @Override
    public IndexFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new IndexFallback();
    }
}
