package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.AntMenuTree;
import timing.ukulele.data.portal.view.AppInfoVO;

import java.util.List;

public interface IIndexFacade {
    @GetMapping("current")
    ResponseData<AppInfoVO> current(@RequestHeader("X-USER") String username,
                                    @RequestHeader(value = "X-TENANT", required = false) Long tenantId,
                                    @RequestParam(value = "type", required = false) Integer type);


    @GetMapping("/current/menu")
    ResponseData<List<AntMenuTree>> currentMenu(@RequestHeader("X-USER") String username,
                                                @RequestHeader(value = "X-TENANT", required = false) Long tenantId,
                                                @RequestParam(value = "type", required = false) Integer type);
}
