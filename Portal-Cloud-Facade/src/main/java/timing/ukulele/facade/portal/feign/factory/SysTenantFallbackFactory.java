package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.SysTenantFallback;

@Slf4j
public class SysTenantFallbackFactory implements FallbackFactory<SysTenantFallback> {
    @Override
    public SysTenantFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new SysTenantFallback();
    }
}
