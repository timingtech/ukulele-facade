package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.IIndexFacade;

@RequestMapping("/index")
public interface IndexControllerFacade extends IIndexFacade {
}
