package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.IDictFacade;
import timing.ukulele.facade.portal.feign.factory.DictFallbackFactory;

@FeignClient(contextId = "dictFeignFacade", name = "ukulele-portal", path = "/portal/dict",
        fallbackFactory = DictFallbackFactory.class)
public interface DictFeignFacade extends IDictFacade {
}
