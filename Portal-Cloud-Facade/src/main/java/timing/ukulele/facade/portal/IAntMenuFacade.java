package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.AntMenuConfigAccessTree;
import timing.ukulele.data.portal.view.AntMenuTreeVO;
import timing.ukulele.data.portal.view.AntMenuVO;

import java.util.List;
import java.util.Map;

public interface IAntMenuFacade {
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return SysDept
     */
    @GetMapping("/{id}")
    ResponseData<AntMenuVO> get(@PathVariable(value = "id") Long id);

    /**
     * 根据参数获取列表
     *
     * @param params 参数集合
     * @return 列表
     */
    @GetMapping("/getByParam")
    ResponseData<List<AntMenuVO>> getByParam(@RequestParam("params") Map<String, Object> params);

    /**
     * 返回树形菜单集合
     *
     * @return 树形菜单
     */
    @GetMapping(value = "/tree")
    ResponseData<List<AntMenuTreeVO>> getMenuTree(@RequestParam(value = "deleted", required = false) Boolean deleted);

    /**
     * 返回业务菜单集合
     *
     * @param deleted 是否删除
     * @return
     */
    @GetMapping(value = "/business/tree")
    ResponseData<List<AntMenuTreeVO>> getBusinessMenuTree(@RequestParam(value = "deleted", required = false) Boolean deleted);

    /**
     * 添加
     *
     * @param sysMenuAnt 实体
     * @return success/false
     */
    @PostMapping()
    ResponseData<Boolean> add(@RequestHeader("X-USER") String currentUser, @RequestBody AntMenuVO sysMenuAnt);

    /**
     * 编辑
     *
     * @param sysMenuAnt 实体
     * @return success/false
     */
    @PutMapping()
    ResponseData<Boolean> edit(@RequestHeader("X-USER") String currentUser, @RequestBody AntMenuVO sysMenuAnt);

    /**
     * 获取角色菜单配置信息，包含已选项、待选项
     *
     * @param roleId 角色id
     * @return 配置信息
     */
    @GetMapping("/role/config/vo")
    ResponseData<List<AntMenuConfigAccessTree>> getRoleMenuConfig(@RequestParam("roleId") Long roleId, @RequestParam(value = "super", required = false) Boolean isSuper);

    /**
     * 配置的菜单及其权限于资源
     *
     * @param config 配置信息
     * @return
     */
    @PostMapping("/role/config/vo/{roleId}")
    ResponseData<Boolean> setRoleMenuConfig(@PathVariable("roleId") Long roleId, @RequestBody List<AntMenuConfigAccessTree> config);

}
