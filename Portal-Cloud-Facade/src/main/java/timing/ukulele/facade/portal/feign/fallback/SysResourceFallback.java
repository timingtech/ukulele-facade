package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.SysResourceGroupTree;
import timing.ukulele.data.portal.view.SysResourceGroupVO;
import timing.ukulele.data.portal.view.SysResourceVO;
import timing.ukulele.facade.portal.feign.SysResourceFeignFacade;

import java.util.List;

public class SysResourceFallback implements SysResourceFeignFacade {
    @Override
    public ResponseData<SysResourceVO> resource(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> add(String currentUser, SysResourceVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> edit(String currentUser, SysResourceVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> delete(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysResourceGroupVO>> getGroupAll(Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysResourceGroupTree>> tree(Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<SysResourceGroupVO> getGroup(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addGroup(String currentUser, SysResourceGroupVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> editGroup(String currentUser, SysResourceGroupVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> disableGroup(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> enableGroup(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
