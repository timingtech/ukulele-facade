package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.ISysMenuPermissionFacade;

@RequestMapping("/menu-permission")
public interface SysMenuPermissionControllerFacade extends ISysMenuPermissionFacade {
}
