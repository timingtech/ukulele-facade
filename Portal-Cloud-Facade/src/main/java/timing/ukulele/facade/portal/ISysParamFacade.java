package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysParamVO;

import java.util.List;
import java.util.Map;

public interface ISysParamFacade {
    /**
     * 通过ID查询参数信息
     *
     * @param id ID
     * @return 参数信息
     */
    @GetMapping("/{id}")
    ResponseData<SysParamVO> get(@PathVariable(value = "id") Long id);

    /**
     * 根据参数获取参数列表
     *
     * @param params 参数集合
     * @return 参数列表
     */
    @GetMapping("getByParam")
    ResponseData<List<SysParamVO>> getParamByParam(@RequestParam("params") Map<String, Object> params);

    /**
     * 根据参数键名获取参数信息
     *
     * @param key 参数键名
     * @return 参数列表
     */
    @GetMapping("/key")
    ResponseData<SysParamVO> getParamByKey(@RequestParam("key") String key, @RequestParam(value = "deleted", required = false) Boolean deleted);


    /**
     * 添加参数
     *
     * @param vo 参数信息
     * @return success、false
     */
    @PostMapping()
    ResponseData<Boolean> add(@RequestHeader("X-USER") String currentUser, @RequestBody SysParamVO vo);

    /**
     * 修改参数
     *
     * @param vo 参数信息
     * @return success/false
     */
    @PutMapping()
    ResponseData<Boolean> update(@RequestHeader("X-USER") String currentUser, @RequestBody SysParamVO vo);

    /**
     * 删除参数
     *
     * @param id 参数ID
     * @return success/fail
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> delete(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

}
