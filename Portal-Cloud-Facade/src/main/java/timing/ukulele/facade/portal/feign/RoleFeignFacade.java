package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.IRoleFacade;
import timing.ukulele.facade.portal.feign.factory.RoleFallbackFactory;

@FeignClient(contextId = "roleFeignFacade", name = "ukulele-portal", path = "/portal/role",
        fallbackFactory = RoleFallbackFactory.class)
public interface RoleFeignFacade extends IRoleFacade {
}
