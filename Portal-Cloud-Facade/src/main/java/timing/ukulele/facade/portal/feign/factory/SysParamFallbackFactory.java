package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.SysParamFeignFacade;
import timing.ukulele.facade.portal.feign.fallback.SysParamFallback;

@Slf4j
public class SysParamFallbackFactory implements FallbackFactory<SysParamFeignFacade> {
    @Override
    public SysParamFeignFacade create(Throwable cause) {
        log.error(cause.getMessage());
        return new SysParamFallback();
    }
}
