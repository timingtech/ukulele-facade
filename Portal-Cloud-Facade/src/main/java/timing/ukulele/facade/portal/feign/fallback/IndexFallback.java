package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.AntMenuTree;
import timing.ukulele.data.portal.view.AppInfoVO;
import timing.ukulele.facade.portal.feign.IndexFeignFacade;

import java.util.List;

public class IndexFallback implements IndexFeignFacade {
    @Override
    public ResponseData<AppInfoVO> current(String username, Long tenantId, Integer type) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<AntMenuTree>> currentMenu(String username, Long tenantId, Integer type) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
