package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.SysMenuPermissionFallback;

@Slf4j
public class SysMenuPermissionFallbackFactory implements FallbackFactory<SysMenuPermissionFallback> {
    @Override
    public SysMenuPermissionFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new SysMenuPermissionFallback();
    }
}
