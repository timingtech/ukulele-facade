package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.RoleFallback;

@Slf4j
public class RoleFallbackFactory implements FallbackFactory<RoleFallback> {
    @Override
    public RoleFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new RoleFallback();
    }
}
