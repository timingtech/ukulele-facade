package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.ISysParamFacade;

@RequestMapping("/param")
public interface SysParamControllerFacade extends ISysParamFacade {
}
