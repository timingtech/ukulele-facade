package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.SysResourceFallback;

@Slf4j
public class SysResourceFallbackFactory implements FallbackFactory<SysResourceFallback> {
    @Override
    public SysResourceFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new SysResourceFallback();
    }
}
