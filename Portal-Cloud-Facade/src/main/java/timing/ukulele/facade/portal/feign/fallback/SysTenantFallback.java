package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysTenantUserVO;
import timing.ukulele.data.portal.view.SysTenantVO;
import timing.ukulele.facade.portal.feign.SysTenantFeignFacade;

import java.util.List;
import java.util.Set;

public class SysTenantFallback implements SysTenantFeignFacade {
    @Override
    public ResponseData<SysTenantVO> get(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysTenantVO>> get(Set<Long> idList) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<SysTenantVO> add(String currentUser, SysTenantVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> delete(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<SysTenantVO> edit(String currentUser, SysTenantVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysTenantVO>> currentUserTenantList(String currentUser) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysTenantVO>> userTenantByUsername(String username) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysTenantVO>> userTenantByUserId(Long userId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<SysTenantVO> checkUserTenant(String username, Long userId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addUserTenant(SysTenantUserVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> deleteUserTenant(Long userId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
