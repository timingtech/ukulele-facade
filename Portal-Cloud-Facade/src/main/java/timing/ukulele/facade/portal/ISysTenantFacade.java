package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysTenantUserVO;
import timing.ukulele.data.portal.view.SysTenantVO;

import java.util.List;
import java.util.Set;

public interface ISysTenantFacade {
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return SysDept
     */
    @GetMapping("/{id}")
    ResponseData<SysTenantVO> get(@PathVariable(value = "id") Long id);

    /**
     * 根据id列表获取
     *
     * @param idList id列表
     * @return
     */
    @PostMapping("/byIdList")
    ResponseData<List<SysTenantVO>> get(@RequestBody Set<Long> idList);

    /**
     * 添加
     *
     * @param vo 实体
     * @return success/false
     */
    @PostMapping()
    ResponseData<SysTenantVO> add(@RequestHeader("X-USER") String currentUser, @RequestBody SysTenantVO vo);

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> delete(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    /**
     * 编辑
     *
     * @param vo 实体
     * @return success/false
     */
    @PutMapping()
    ResponseData<SysTenantVO> edit(@RequestHeader("X-USER") String currentUser, @RequestBody SysTenantVO vo);

    /**
     * 当前用户的所有租户
     * @param currentUser
     * @return
     */
    @GetMapping("/user/tenant")
    ResponseData<List<SysTenantVO>> currentUserTenantList(@RequestHeader("X-USER") String currentUser);

    /**
     * 用户所加入的租户
     *
     * @param username 用户名
     * @return
     */
    @GetMapping("/user/tenant/username")
    ResponseData<List<SysTenantVO>> userTenantByUsername(@RequestParam("username") String username);

    /**
     * 用户所加入的租户
     *
     * @param userId 用户id
     * @return
     */
    @GetMapping("/user/tenant/id")
    ResponseData<List<SysTenantVO>> userTenantByUserId(@RequestParam("userId") Long userId);

    /**
     * 检查用户是否存在于租户中
     *
     * @param username 用户名
     * @param userId   用户id
     * @param tenantId 机构id
     * @return SysTenantVO对象或者空
     */
    @GetMapping("/user/tenant/check")
    ResponseData<SysTenantVO> checkUserTenant(@RequestParam(value = "username", required = false) String username, @RequestParam(value = "userId", required = false) Long userId, @RequestParam("tenantId") Long tenantId);

    /**
     * 添加租户用户
     *
     * @param vo 租户用户信息
     * @return
     */
    @PostMapping("/user/tenant")
    ResponseData<Boolean> addUserTenant(@RequestBody SysTenantUserVO vo);

    /**
     * 删除租户用户
     *
     * @param userId   用户id
     * @param tenantId 租户id
     * @return
     */
    @DeleteMapping("/user/tenant")
    ResponseData<Boolean> deleteUserTenant(@RequestParam("userId") Long userId, @RequestParam("tenantId") Long tenantId);
}
