package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.IAntMenuFacade;

@RequestMapping("/ant-menu")
public interface AntMenuControllerFacade extends IAntMenuFacade {
}
