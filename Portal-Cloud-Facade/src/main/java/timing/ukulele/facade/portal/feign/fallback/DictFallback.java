package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.DictIndexVO;
import timing.ukulele.data.portal.view.DictVO;
import timing.ukulele.facade.portal.IDictFacade;

import java.util.List;

public class DictFallback implements IDictFacade {
    @Override
    public ResponseData<DictVO> dict(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> dict(String currentUser, DictVO sysDict) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> editDict(String currentUser, DictVO sysDict) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> deleteDict(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<DictVO>> findDictByIndex(String key) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<DictIndexVO> dictIndex(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> dictIndex(String currentUser, DictIndexVO sysDictIndex) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> editDictIndex(String currentUser, DictIndexVO sysDict) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> deleteDictIndex(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
