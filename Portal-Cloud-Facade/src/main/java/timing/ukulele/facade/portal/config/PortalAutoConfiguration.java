package timing.ukulele.facade.portal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import timing.ukulele.facade.portal.feign.factory.*;

@Configuration
public class PortalAutoConfiguration {
    @Bean
    public AntMenuFallbackFactory antMenuFallbackFactory() {
        return new AntMenuFallbackFactory();
    }

    @Bean
    public DictFallbackFactory dictFallbackFactory() {
        return new DictFallbackFactory();
    }

    @Bean
    public RoleFallbackFactory roleFallbackFactory() {
        return new RoleFallbackFactory();
    }

    @Bean
    public IndexFallbackFactory indexFallbackFactory() {
        return new IndexFallbackFactory();
    }

    @Bean
    public SysMenuPermissionFallbackFactory sysMenuPermissionFactory() {
        return new SysMenuPermissionFallbackFactory();
    }

    @Bean
    public SysResourceFallbackFactory sysResourceFactory() {
        return new SysResourceFallbackFactory();
    }

    @Bean
    public SysParamFallbackFactory sysParamFallbackFactory() {
        return new SysParamFallbackFactory();
    }

    @Bean
    public SysTenantFallbackFactory sysTenantFallbackFactory() {
        return new SysTenantFallbackFactory();
    }
}
