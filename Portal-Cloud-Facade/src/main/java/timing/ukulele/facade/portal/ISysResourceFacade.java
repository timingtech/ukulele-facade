package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.SysResourceGroupTree;
import timing.ukulele.data.portal.view.SysResourceGroupVO;
import timing.ukulele.data.portal.view.SysResourceVO;

import java.util.List;

public interface ISysResourceFacade {
    /**
     * 通过ID查询信息
     *
     * @param id ID
     * @return 资源信息
     */
    @GetMapping("/{id}")
    ResponseData<SysResourceVO> resource(@PathVariable(value = "id") Long id);

    /**
     * 添加资源
     *
     * @param vo 资源信息
     * @return success、false
     */
    @PostMapping()
    ResponseData<Boolean> add(@RequestHeader("X-USER") String currentUser, @RequestBody SysResourceVO vo);

    /**
     * 修改资源
     *
     * @param vo 权限资源
     * @return success/false
     */
    @PutMapping()
    ResponseData<Boolean> edit(@RequestHeader("X-USER") String currentUser, @RequestBody SysResourceVO vo);

    /**
     * 删除资源
     *
     * @param id id
     * @return
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> delete(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    //==============================资源组接口================================\\

    /**
     * 通过ID查询组信息
     *
     * @param id ID
     * @return 资源信息
     */
    @GetMapping("/group/{id}")
    ResponseData<SysResourceGroupVO> getGroup(@PathVariable(value = "id") Long id);

    /**
     * 获取所有资源组
     *
     * @param deleted 删除标记
     * @return 所有资源组
     */
    @GetMapping("/group")
    ResponseData<List<SysResourceGroupVO>> getGroupAll(@RequestParam(value = "deleted", required = false) Boolean deleted);

    /**
     * 获取资源组树
     *
     * @param deleted 删除标记
     * @return 所有资源组
     */
    @GetMapping("/group/tree")
    ResponseData<List<SysResourceGroupTree>> tree(@RequestParam(value = "deleted", required = false) Boolean deleted);

    /**
     * 添加资源组
     *
     * @param vo 资源组信息
     * @return success、false
     */
    @PostMapping("/group")
    ResponseData<Boolean> addGroup(@RequestHeader("X-USER") String currentUser, @RequestBody SysResourceGroupVO vo);

    /**
     * 修改资源组
     *
     * @param vo 编辑资源组
     * @return success/false
     */
    @PutMapping("/group")
    ResponseData<Boolean> editGroup(@RequestHeader("X-USER") String currentUser, @RequestBody SysResourceGroupVO vo);

    /**
     * 删除资源组
     *
     * @param id id
     * @return 操作结果
     */
    @DeleteMapping("/group/disable/{id}")
    ResponseData<Boolean> disableGroup(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    /**
     * 启用资源组
     *
     * @param id id
     * @return 操作结果
     */
    @DeleteMapping("/group/enable/{id}")
    ResponseData<Boolean> enableGroup(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

}
