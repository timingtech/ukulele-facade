package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.ISysTenantFacade;
import timing.ukulele.facade.portal.feign.factory.SysTenantFallbackFactory;

@FeignClient(contextId = "sysTenantFeignFacade", name = "ukulele-portal", path = "/portal/tenant",
        fallbackFactory = SysTenantFallbackFactory.class)
public interface SysTenantFeignFacade extends ISysTenantFacade {
}
