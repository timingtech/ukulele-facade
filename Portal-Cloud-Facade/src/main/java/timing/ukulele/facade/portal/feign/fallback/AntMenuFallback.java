package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.data.AntMenuConfigAccessTree;
import timing.ukulele.data.portal.view.AntMenuTreeVO;
import timing.ukulele.data.portal.view.AntMenuVO;
import timing.ukulele.facade.portal.IAntMenuFacade;

import java.util.List;
import java.util.Map;

public class AntMenuFallback implements IAntMenuFacade {
    @Override
    public ResponseData<AntMenuVO> get(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<AntMenuVO>> getByParam(Map<String, Object> params) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<AntMenuTreeVO>> getMenuTree(Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<AntMenuTreeVO>> getBusinessMenuTree(Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> add(String currentUser, AntMenuVO sysMenuAnt) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> edit(String currentUser, AntMenuVO sysMenuAnt) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }


    @Override
    public ResponseData<List<AntMenuConfigAccessTree>> getRoleMenuConfig(Long roleId, Boolean isSuper) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> setRoleMenuConfig(Long roleId, List<AntMenuConfigAccessTree> config) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
