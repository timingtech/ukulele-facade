package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.IDictFacade;

@RequestMapping("/dict")
public interface DictControllerFacade extends IDictFacade {
}
