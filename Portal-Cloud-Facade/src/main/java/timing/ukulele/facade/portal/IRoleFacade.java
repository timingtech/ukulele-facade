package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.RoleVO;
import timing.ukulele.data.portal.view.SysResourceVO;
import timing.ukulele.data.portal.view.TenantUserRoleVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IRoleFacade {
    //==========================Role实体操作===============================//

    /**
     * 通过ID查询角色信息
     *
     * @param id ID
     * @return 角色信息
     */
    @GetMapping("/{id}")
    ResponseData<RoleVO> role(@PathVariable(value = "id") Long id);

    /**
     * 根据参数获取角色列表
     *
     * @param params 参数集合
     * @return 角色列表
     */
    @GetMapping("/getByParam")
    ResponseData<List<RoleVO>> getRoleByParam(@RequestParam("params") Map<String, Object> params);

    /**
     * 获取租户的角色
     *
     * @param tenantId 租户ID
     * @return
     */
    @GetMapping("/tenant")
    ResponseData<List<RoleVO>> getTenantRole(@RequestParam("tenantId") Long tenantId,
                                             @RequestParam(value = "code", required = false) String code,
                                             @RequestParam(value = "name", required = false) String name,
                                             @RequestParam(value = "deleted", required = false) Boolean deleted);

    /**
     * 添加角色
     *
     * @param role 角色信息
     * @return success、false
     */
    @PostMapping()
    ResponseData<RoleVO> role(@RequestHeader("X-USER") String currentUser, @RequestBody RoleVO role);

    /**
     * 修改角色
     *
     * @param role 角色信息
     * @return success/false
     */
    @PutMapping()
    ResponseData<Boolean> roleUpdate(@RequestHeader("X-USER") String currentUser, @RequestBody RoleVO role);

    /**
     * 删除角色
     *
     * @param id 角色ID
     * @return success/fail
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> roleDel(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    /**
     * 添加角色并且根据编码拷贝菜单和资源
     *
     * @param role 角色信息
     * @return success、false
     */
    @PostMapping("/addRoleFollowCopyMenuAndResource")
    ResponseData<RoleVO> addRoleFollowCopyMenuAndResource(@RequestHeader("X-USER") String currentUser, @RequestBody RoleVO role);

    /**
     * 获取用户已具备的角色列表
     *
     * @param userId 用户id
     * @return 角色列表
     */
    @GetMapping("/user/{userId}")
    ResponseData<List<RoleVO>> getUserRole(@PathVariable(value = "userId") Long userId, @RequestHeader(value = "X-TENANT", required = false) Long tenantId);

    /**
     * 删除用户角色
     *
     * @param userId 用户ID
     * @param roleId 角色ID 若不传则删除用户所有角色
     * @return 成功/失败
     */
    @DeleteMapping("/user/{userId}/{roleId}")
    ResponseData<Boolean> deleteUserRole(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "userId") Long userId,
                                         @PathVariable(value = "roleId", required = false) Long roleId, @RequestHeader(value = "X-TENANT", required = false) Long tenantId);

    /**
     * 添加用户角色
     *
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 成功/失败
     */
    @PostMapping("/user/{userId}/{roleId}")
    ResponseData<Boolean> addUserRole(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "userId") Long userId, @RequestParam(value = "username") String username,
                                      @PathVariable(value = "roleId") Long roleId, @RequestHeader(value = "X-TENANT", required = false) Long tenantId);

//    /**
//     * 添加用户在某机构下的角色
//     *
//     * @param userId   用户id
//     * @param roleCode 角色列表
//     * @param tenantId 租户id
//     * @return 成功/失败
//     */
//    @PostMapping("/user/tenant/{userId}")
//    ResponseData<Boolean> addTenantUserRole(@PathVariable(value = "userId") Long userId, @RequestParam("username") String username,
//                                            @RequestParam("roleCode") List<String> roleCode, @RequestHeader(value = "X-TENANT") Long tenantId);
//
//    /**
//     * 删除用户在某机构下的角色
//     *
//     * @param userId   用户id
//     * @param tenantId 租户id
//     * @return 成功/失败
//     */
//    @DeleteMapping("/user/tenant/{userId}")
//    ResponseData<Boolean> deleteTenantUserRole(@PathVariable(value = "userId") Long userId, @RequestHeader(value = "X-TENANT", required = false) Long tenantId);


    /**
     * 修改租户用户角色
     *
     * @param tenantId 租户id
     * @return
     */
    @PostMapping("/user/tenant")
    ResponseData<Boolean> changeTenantUserRole(@RequestHeader(value = "X-TENANT") Long tenantId, @RequestBody TenantUserRoleVO vo);

    /**
     * 添加用户角色和用户租户
     * @param currentUser
     * @param userId
     * @param username
     * @param roleId
     * @param tenantId
     * @return
     */
    @PostMapping("/addUserRoleAndTenant")
    ResponseData<Boolean> addUserRoleAndTenant(@RequestHeader("X-USER") String currentUser, @RequestParam(value = "userId") Long userId, @RequestParam(value = "username") String username,
                                               @RequestParam(value = "roleId") Long roleId, @RequestHeader(value = "X-TENANT") Long tenantId);

    /**
     * 处理用户注册成功后的逻辑
     *
     * @param userId 用户id
     * @return
     */
    @PostMapping("/user/registered/{userId}")
    ResponseData<Boolean> userRegistered(@PathVariable(value = "userId") Long userId, @RequestParam("username") String username);


    //=============================角色资源===================================\\

    /**
     * 通过角色id查找资源
     *
     * @param roleId 角色id
     * @return 角色资源
     */
    @GetMapping("/resource/{roleId}")
    ResponseData<List<SysResourceVO>> findByRole(@PathVariable(value = "roleId") Long roleId);

    /**
     * 查询用户所有的资源及其请求方法
     *
     * @param username 用户名
     * @param tenantId 租户id
     * @return
     */
    @GetMapping("/resource/tenant/{username}")
    ResponseData<Map<String, Set<String>>> findByUsername(@PathVariable(value = "username") String username, @RequestHeader("X-TENANT") Long tenantId);

    /**
     * 查询一组角色的资源合集
     *
     * @param roleIds 资源id数组
     * @return 该组角色的资源合集
     */
    @GetMapping("/resource")
    ResponseData<List<SysResourceVO>> findByRoles(@RequestParam(value = "roleId[]") List<Long> roleIds);

    /**
     * 查询一组角色的角色详细信息
     *
     * @param roleIds 资源id数组
     * @return 该组角色的资源合集
     */
    @GetMapping("/resource/roles")
    ResponseData<List<RoleVO>> findRolesByIds(@RequestParam(value = "roleId[]") List<Long> roleIds);

    @PostMapping("/resource/{roleId}")
    ResponseData<Boolean> addRoleResource(@PathVariable(value = "roleId") Long roleId, @RequestBody List<Long> resourceIdList);

    @DeleteMapping("/resource/{roleId}")
    ResponseData<Boolean> deleteRoleResource(@PathVariable(value = "roleId") Long roleId, @RequestParam("resourceId") Long resourceId);
}
