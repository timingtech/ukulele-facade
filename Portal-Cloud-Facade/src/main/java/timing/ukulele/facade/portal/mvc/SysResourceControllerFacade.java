package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.ISysResourceFacade;

@RequestMapping("/resource")
public interface SysResourceControllerFacade extends ISysResourceFacade {
}
