package timing.ukulele.facade.portal;

import org.springframework.web.bind.annotation.*;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysMenuPermissionVO;

import java.util.List;

public interface ISysMenuPermissionFacade {
    /**
     * 通过ID查询信息
     *
     * @param id ID
     * @return 权限信息
     */
    @GetMapping("/{id}")
    ResponseData<SysMenuPermissionVO> permission(@PathVariable(value = "id") Long id);

    /**
     * 添加权限
     *
     * @param vo 权限信息
     * @return success、false
     */
    @PostMapping()
    ResponseData<Boolean> add(@RequestHeader("X-USER") String currentUser, @RequestBody SysMenuPermissionVO vo);

    /**
     * 修改权限
     *
     * @param vo 权限信息
     * @return success/false
     */
    @PutMapping()
    ResponseData<Boolean> edit(@RequestHeader("X-USER") String currentUser, @RequestBody SysMenuPermissionVO vo);

    /**
     * 删除权限
     *
     * @param id id
     * @return
     */
    @DeleteMapping("/{id}")
    ResponseData<Boolean> delete(@RequestHeader("X-USER") String currentUser, @PathVariable(value = "id") Long id);

    /**
     * 通过菜单id查找权限
     *
     * @param menuId 菜单id
     * @return 菜单权限
     */
    @GetMapping("/menu/{menuId}")
    ResponseData<List<SysMenuPermissionVO>> findByMenu(@PathVariable(value = "menuId") Long menuId,
                                                       @RequestParam(value = "deleted", required = false) Boolean deleted);

}
