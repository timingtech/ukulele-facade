package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.ISysResourceFacade;
import timing.ukulele.facade.portal.feign.factory.SysResourceFallbackFactory;

@FeignClient(contextId = "sysResourceFeignFacade", name = "ukulele-portal", path = "/portal/resource",
        fallbackFactory = SysResourceFallbackFactory.class)
public interface SysResourceFeignFacade extends ISysResourceFacade {
}
