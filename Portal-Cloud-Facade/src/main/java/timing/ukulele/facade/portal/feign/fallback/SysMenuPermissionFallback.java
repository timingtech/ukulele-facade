package timing.ukulele.facade.portal.feign.fallback;

import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.SysMenuPermissionVO;
import timing.ukulele.facade.portal.feign.SysMenuPermissionFeignFacade;

import java.util.List;

public class SysMenuPermissionFallback implements SysMenuPermissionFeignFacade {
    @Override
    public ResponseData<SysMenuPermissionVO> permission(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> add(String currentUser, SysMenuPermissionVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> edit(String currentUser, SysMenuPermissionVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> delete(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysMenuPermissionVO>> findByMenu(Long menuId, Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
