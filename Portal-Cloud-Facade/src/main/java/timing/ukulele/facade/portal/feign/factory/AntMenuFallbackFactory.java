package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.AntMenuFallback;

@Slf4j
public class AntMenuFallbackFactory implements FallbackFactory<AntMenuFallback> {
    @Override
    public AntMenuFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new AntMenuFallback();
    }
}
