package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.ISysParamFacade;
import timing.ukulele.facade.portal.feign.factory.SysParamFallbackFactory;

@FeignClient(contextId = "sysParamFeignFacade", name = "ukulele-portal", path = "/portal/param",
        fallbackFactory = SysParamFallbackFactory.class)
public interface SysParamFeignFacade extends ISysParamFacade {
}
