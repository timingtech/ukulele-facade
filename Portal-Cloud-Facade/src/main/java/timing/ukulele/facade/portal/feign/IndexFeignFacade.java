package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.IIndexFacade;
import timing.ukulele.facade.portal.feign.factory.IndexFallbackFactory;

@FeignClient(contextId = "indexFeignFacade", name = "ukulele-portal", path = "/portal/index",
        fallbackFactory = IndexFallbackFactory.class)
public interface IndexFeignFacade extends IIndexFacade {
}
