package timing.ukulele.facade.portal.mvc;

import org.springframework.web.bind.annotation.RequestMapping;
import timing.ukulele.facade.portal.IRoleFacade;

@RequestMapping("/role")
public interface RoleControllerFacade extends IRoleFacade {
}
