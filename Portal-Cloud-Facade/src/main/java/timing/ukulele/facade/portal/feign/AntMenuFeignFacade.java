package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.IAntMenuFacade;
import timing.ukulele.facade.portal.feign.factory.AntMenuFallbackFactory;

@FeignClient(contextId = "antMenuFeignFacade", name = "ukulele-portal", path = "/portal/ant-menu",
        fallbackFactory = AntMenuFallbackFactory.class)
public interface AntMenuFeignFacade extends IAntMenuFacade {
}
