package timing.ukulele.facade.portal.feign.fallback;

import org.springframework.web.bind.annotation.RequestParam;
import timing.ukulele.common.data.ResponseCode;
import timing.ukulele.common.data.ResponseData;
import timing.ukulele.data.portal.view.RoleVO;
import timing.ukulele.data.portal.view.SysResourceVO;
import timing.ukulele.data.portal.view.TenantUserRoleVO;
import timing.ukulele.facade.portal.IRoleFacade;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class RoleFallback implements IRoleFacade {
    @Override
    public ResponseData<RoleVO> role(Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<RoleVO>> getRoleByParam(Map<String, Object> params) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<RoleVO>> getTenantRole(Long tenantId, String code, String name, Boolean deleted) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<RoleVO> role(String currentUser, RoleVO role) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> roleUpdate(String currentUser, RoleVO role) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> roleDel(String currentUser, Long id) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<RoleVO> addRoleFollowCopyMenuAndResource(String currentUser, RoleVO role) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<RoleVO>> getUserRole(Long userId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> deleteUserRole(String currentUser, Long userId, Long roleId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addUserRole(String currentUser, Long userId, String username, Long roleId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> changeTenantUserRole(Long tenantId, TenantUserRoleVO vo) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addUserRoleAndTenant(String currentUser, Long userId, String username, Long roleId, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> userRegistered(Long userId, String username) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysResourceVO>> findByRole(Long roleId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Map<String, Set<String>>> findByUsername(String username, Long tenantId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<SysResourceVO>> findByRoles(List<Long> roleIds) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<List<RoleVO>> findRolesByIds(List<Long> roleIds) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> addRoleResource(Long roleId, List<Long> resourceIdList) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }

    @Override
    public ResponseData<Boolean> deleteRoleResource(Long roleId, Long resourceId) {
        return new ResponseData<>(ResponseCode.FACADE_ERROR);
    }
}
