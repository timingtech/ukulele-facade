package timing.ukulele.facade.portal.feign.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import timing.ukulele.facade.portal.feign.fallback.DictFallback;

@Slf4j
public class DictFallbackFactory implements FallbackFactory<DictFallback> {
    @Override
    public DictFallback create(Throwable cause) {
        log.error(cause.getMessage());
        return new DictFallback();
    }
}
