package timing.ukulele.facade.portal.feign;

import org.springframework.cloud.openfeign.FeignClient;
import timing.ukulele.facade.portal.ISysMenuPermissionFacade;
import timing.ukulele.facade.portal.feign.factory.SysMenuPermissionFallbackFactory;

@FeignClient(contextId = "sysMenuPermissionFeignFacade", name = "ukulele-portal", path = "/portal/menu-permission",
        fallbackFactory = SysMenuPermissionFallbackFactory.class)
public interface SysMenuPermissionFeignFacade extends ISysMenuPermissionFacade {
}
