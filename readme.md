# 项目简介
本项目是Ukulele快速框架的接口规范组件项目，服务间调用仅需因此此项目中的对应模块接口

# 快速开发框架地址
[ukulele-cloud-nacos](https://gitee.com/timingtech/ukulele-cloud-nacos)

# 分支说明
* master：主分支(暂时无用)
* feign：使用feign作为服务调用客户端的分支
* dubbo：使用Dubbo作为服务调用客户端的分支（待开发）

# 模块说明
* Auth-Cloud-Facade：Auth2授权模块接口规范
* Portal-Cloud-Facade：系统服务接口规范
* User-Cloud-Facade：用户服务接口规范


**已经看到这里了，留个star呗**